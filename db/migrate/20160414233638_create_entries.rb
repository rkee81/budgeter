class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.decimal :amount
      t.string :description
      t.boolean :deductible

      t.timestamps null: false
    end
  end
end
