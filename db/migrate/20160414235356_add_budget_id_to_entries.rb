class AddBudgetIdToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :budget_id, :integer
  end
end
