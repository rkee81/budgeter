class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.string :name
      t.string :description
      t.decimal :balance

      t.timestamps null: false
    end
  end
end
