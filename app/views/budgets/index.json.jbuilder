json.array!(@budgets) do |budget|
  json.extract! budget, :id, :name, :description, :balance
  json.url budget_url(budget, format: :json)
end
