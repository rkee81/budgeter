class Budget < ActiveRecord::Base
  has_many :entries, :dependent => :delete_all
  validates :balance, :presence => true

  def get_budget_total
  	if self.balance
      newtotal = balance
        entries.each do |entry|
          newtotal += entry.amount
        end
      newtotal
    end
  end
end
